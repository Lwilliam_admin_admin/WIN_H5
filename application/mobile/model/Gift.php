<?php
/**
 * Created by PhpStorm.
 * User: lgw
 * Date: 2019-03-08
 * Time: 12:17
 */
namespace  app\mobile\model;

use think\Model;

class Gift extends Model{
    protected $table = 'show_gift';
    protected  $pk = 'id';
    public function getAll(){
        return $this->select();
    }
}