<?php
namespace app\mobile\controller;
use app\common\logic\WechatLogic;
use think\Db;

class Weixin  {
    public function index()
    {
        $config = Db::name('wx_config')->find();
        if ($config['wait_access'] == 0) {
            ob_clean();
            exit($_GET["echostr"]);
        }
        $logic = new WechatLogic($config);
        $logic->handleMessage();

    }

}