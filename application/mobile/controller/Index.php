<?php

namespace app\mobile\controller;
use app\admin\model\Gift;
use app\api\controller\Api;
use app\mobile\model\WxUser;
use think\Controller;
use Think\Db;
use think\Session;

class Index extends Controller {
    protected  $model ;
    public function _initialize(){
        parent::_initialize();
        header(	'Content-Type: text/html; charset=utf-8');
        if(!is_weixin()){
            exit('请通过微信浏览器打开');
        }
        $config = F("config");
        if(empty($config)){
            $config = cache_config();
        }
        if($config['end'] < time() ){
            exit('活动已结束');
        }
        if($config['begin'] > time() ){
            exit('活动未开始');
        }
        if(!$config['close_open']){
            exit('网站已关闭');
        }
        $protocol = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";
        $url = "$protocol$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
        $wx_config = Db::name('wx_config')->find();
        $jssdk = new \app\mobile\logic\JSSDKLogic($wx_config['appid'],$wx_config['appsecret']);
        $signPackage = $jssdk->GetSignPackage();
        $this->assign('signPackage', $signPackage);
        $data_share['title'] = $config['page_title'];
        $data_share['desc'] =  $config['page_desc'];
        $data_share['link'] = $config['web_url'].'/index.php/mobile/index/index';
        $images = $config['shareImg'];
        $data_share['imgUrl'] = $config['web_url'].$images;
        $this->assign('data_share',$data_share);
        $this->assign('store_title',$config['store_title']);
        $this->assign('url',$url);
    }

    public function _empty($index){
        return $this->fetch($index);
    }




}