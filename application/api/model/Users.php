<?php
/**
 * Created by PhpStorm.
 * User: lgw
 * Date: 2019-03-07
 * Time: 11:55
 */
namespace app\api\model;

use think\Model;

class Users extends Model
{
  protected $table = 'tp_users';
  protected  $pk = 'id';

}