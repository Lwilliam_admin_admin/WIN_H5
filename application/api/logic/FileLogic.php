<?php
namespace app\api\logic;
use think\Loader;
use think\Model;
use think\Db;
class FileLogic{
    /*导入数据*/
    function goods_import($filename, $exts='xls')
    {
        //导入PHPExcel类库，因为PHPExcel没有用命名空间，
        Vendor("PHPExcel.Classes.PHPExcel");
        Vendor("PHPExcel.Classes.PHPExcel.IOFactory");
        //如果excel文件后缀名为.xls，导入这个类
        if($exts == 'xls'){
            Vendor("PHPExcel.Classes.PHPExcel.Reader.Excel5");
            $PHPReader = new \PHPExcel_Reader_Excel5();
        }else if($exts == 'xlsx'){
            Vendor("PHPExcel.Classes.PHPExcel.Reader.Excel2007");
            $PHPReader=new \PHPExcel_Reader_Excel2007();
        }
        //载入文件
        $PHPExcel = $PHPReader->Load($filename);
        //获取工作表的数目
        $data = $PHPExcel->getSheet(0)->toArray(null,true,true,true);
        return $data;
        }

}



?>