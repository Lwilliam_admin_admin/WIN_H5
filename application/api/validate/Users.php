<?php
namespace app\api\validate;
use think\Validate;
use think\Db;
class Users extends Validate
{
    // 验证规则
    protected $rule = [
        'username'            => 'require',
        'address'            => 'require',
        'phone'            => 'require',
    ];
    //错误信息
    protected $message  = [
        'username.require'                                => '用户名必填',
        'address.unique'                                 =>'地址必填',
        'phone.require'                                => '手机号必填',
    ];

}