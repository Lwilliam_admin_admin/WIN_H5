<?php
namespace app\api\controller;
use app\api\model\Users;
use app\mobile\controller\MobileBase;
use app\mobile\model\Gift;
use app\mobile\model\WxUser;
use think\Controller;
use think\Db;
use think\File;
use think\Loader;
use think\Session;

class Api extends Controller {
    const CODE_OK = 200;   //成功
    const CODE_FAIL = 400;//失败
    const CODE_AREA_NOT = 402;
    const CODE_HAS_TASK= 405;
    const CODE_NOT_FIND = 404;//没找到
    const CODE_NOT_ACCESS = 403;//禁止
    const CODE_ACCESS = 500; //内部服务器错误
    const CODE_UNAUTHORIZED = 401;//未经授权
    const CODE_GATEWAY_TIMEOUT = 504; //网关超时
    const CODE_API1 = 10001;
    const CODE_API2 = 10002;
    const CODE_API3 = 10003;
    public function _initialize()
    {
        Session::start();
        if (session('?user')) {
            $session_user = session('user');
            $user = M('wx_user')->where(['user_id'=>$session_user['user_id']])->find();
            session('user', $user);
        }
    }

    public static function apiResponse($data,$code=self::CODE_OK,$message='获取成功'){
        $res = [
            'code'=>$code,
            'data'=>$data,
            'message'=>$message
        ];
        exit(json_encode($res,JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES|JSON_NUMERIC_CHECK));
    }

    public static function getuser($uid) {
        $m = M ( "wx_user" );
        $where["openid"] = $uid;
        $result = $m->where($where)->find();
        if ($result)   return $result;
    }

    //是否有中奖信息 或 已填过地址
    public function check_user(){
        $list  = Users::where(['openid'=>session('user.openid'),'status'=>0])->order('desc')->find();
        if(empty($list)){
            Api::apiResponse(1,self::CODE_NOT_ACCESS,'没有中奖信息');
        }
    }

    public function get_reward(){
        if(IS_POST){
            $config_percent = tpCache('shop_info');
            $config_percent = $config_percent['thinks'];
            if($config_percent == 1){
                $int = 100;
                $p = 100;
            }else{
                $c_percent = $config_percent * 100;
                $p = $c_percent;
                $int = 100;
            }
            $number = mt_rand(1,$int);
            if($number <= $p) {
                $this->add_userinfo();
            }else{
                //未中奖
                Api::apiResponse(1,self::CODE_FAIL,'未中奖');
            }
        }
    }

    public function add_userinfo(){
        if(IS_POST){
            $users  = new Users();
            $users->openid = session('user.openid');
            $users->add_time = time();
            $users->save();
            Api::apiResponse(1,self::CODE_OK,'快去填写资料吧');
        }
    }
    public function add_address(){
        if(IS_POST){
            $list  = Users::where(['openid'=>session('user.openid'),'status'=>0])->order('desc')->find();
            if($list){
                $data = I('post.');
                $validate = Loader::validate('Users');
                if (!$validate->batch()->check($data)) {
                    $error = $validate->getError();
                    $error_msg = array_values($error);
                    self::apiResponse('1',self::CODE_ACCESS,$error_msg?$error_msg:'提交信息有误');
                }
                $data['get_time'] = time();
                Users::update($data,['id'=>$list['id'],'openid'=>session('user.openid')]);
                Api::apiResponse(1,self::CODE_OK,'提交成功');
            }else{
                Api::apiResponse(1,self::CODE_NOT_ACCESS,'没有中奖信息');
            }
        }
    }


}


?>