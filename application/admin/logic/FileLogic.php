<?php
namespace app\admin\logic;
use think\Loader;
use think\Model;
use think\Db;
use PHPExcel_IOFactory;
use PHPExcel;
class FileLogic{
    /*导入数据*/
    function goods_import($filename, $exts='xls')
    {
        //导入PHPExcel类库，因为PHPExcel没有用命名空间，
        Vendor("PHPExcel.Classes.PHPExcel");
        Vendor("PHPExcel.Classes.PHPExcel.IOFactory");
        //如果excel文件后缀名为.xls，导入这个类
        if($exts == 'xls'){
            Vendor("PHPExcel.Classes.PHPExcel.Reader.Excel5");
            $PHPReader=new \PHPExcel_Reader_Excel5();
        }else if($exts == 'xlsx'){
            Vendor("PHPExcel.Classes.PHPExcel.Reader.Excel2007");
            $PHPReader=new \PHPExcel_Reader_Excel2007();
        }
        $data = array();
        //载入文件
        $PHPExcel=$PHPReader->load($filename);
        //获取表中的第一个工作表，如果要获取第二个，把0改为1，依次类推
        $currentSheet=$PHPExcel->getSheet(0);
        //获取总列数
        $allColumn=$currentSheet->getHighestColumn();
        //获取总行数
        $allRow=$currentSheet->getHighestRow();
        //p($allRow);die;
        //循环获取表中的数据，$currentRow表示当前行，从哪行开始读取数据，索引值从0开始
        for($currentRow=1;$currentRow<=$allRow;$currentRow++){
            //从哪列开始，A表示第一列
            for($currentColumn='A';$currentColumn<=$allColumn;$currentColumn++){
                //数据坐标
                $address=$currentColumn.$currentRow;
                //p($currentRow);
                if(!empty($currentSheet->getCell($address)->getValue())){
                    $data[$currentRow-1]= $currentSheet->getCell($address)->getValue();
                }
               //  p( $data[$currentRow-1]);
            }
        }

      //  p( $data);die;
        return $data;
      //  $data=$PHPExcel->getSheet(0)->toArray(null,true,true,true);
       // $this->balance_sheet($data);

    }
}


?>