<?php
return	array(	
	'system'=>array('name'=>'系统','child'=>array(
			array('name' => '设置','child' => array(
					array('name'=>'网站设置','act'=>'index','op'=>'System'),
			)),
		/*	array('name' => '礼品管理','child'=>array(
                    array('name'=>'礼品','act'=>'reduce','op'=>'Gift'),
			)),*/
        array('name' => '用户管理','child'=>array(
            array('name'=>'用户','act'=>'index','op'=>'Users'),
        )),
         array('name' => '微信','child' => array(
              array('name' => '公众号配置', 'act'=>'index', 'op'=>'Wechat'),
          )),
			array('name' => '数据','child'=>array(
				array('name' => '数据备份', 'act'=>'index', 'op'=>'Tools'),
				array('name' => '数据还原', 'act'=>'restore', 'op'=>'Tools'),
			))
	)),

);