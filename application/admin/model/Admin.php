<?php
/**
 * Created by PhpStorm.
 * User: lgw
 * Date: 2019-03-07
 * Time: 11:55
 */
namespace app\admin\model;

use think\Model;

class Admin extends Model
{
  protected $table = 'show_admin';
  protected  $pk = 'id';
  public function getAll(){
      return $this->select();
  }
}