<?php
namespace app\admin\controller;
use app\admin\controller\Base;
use app\admin\logic\FileLogic;
use app\mobile\model\WxUser;
use think\AjaxPage;
class Users extends Base {
    public function changeTableVal(){
        $table = I('table'); // 表名
        $id_name = I('id_name'); // 表主键id名
        $id_value = I('id_value'); // 表主键id值
        $field  = I('field'); // 修改哪个字段
        $value  = I('value'); // 修改字段值
        $yuan  = M($table)->where("$id_name = $id_value")->find();
        $add_num = $value - $yuan['all_number'];
        $number = $yuan['number'] + $add_num;
        M($table)->where("$id_name = $id_value")->save(array($field=>$value,'number'=>$number)); // 根据条件保存修改的数据
    }

    public function index(){
        return $this->fetch();
    }
    public function wechat_ajax(){
        // 搜索条件
        $condition = array();
        $sort_order = 'add_time desc ,id desc';
        I('keywords') ? $condition['username'] = I('keywords') : false;
        $now_time = time();
        //当天 0 点
        $zero  = strtotime(date('Y-m-d 00:00:00',$now_time));
        //当天12 点
        $night  = strtotime(date('Y-m-d 23:59:59',$now_time));
        $model = M('users');
        $count = $model->where($condition)->count();
        $Page  = new AjaxPage($count,10);
        $userList = $model->where($condition)->order($sort_order)->limit($Page->firstRow.','.$Page->listRows)->select();
        foreach($userList as $k=>$v){
            if( ( $v['add_time'] > $zero ) &&  ($v['add_time']< $night )){
                $userList[$k]['flag']  = '今天';
            }else{
                $userList[$k]['flag']  ='';
            }
        }
        $show = $Page->show();
        $this->assign('userList',$userList);
        $this->assign('page',$show);// 赋值分页输出
        $this->assign('pager',$Page);
        return $this->fetch();
    }

    //未使用
    public function rest_data(){
        M('users')->where('1=1')->save(['add_time'=>'']);
        $return_arr = array('status' => 1,'msg' => '操作成功','data'  =>'');
        $this->ajaxReturn($return_arr);
    }

    public function del_all(){
        M('users')->where('1=1')->delete();
        $return_arr = array('status' => 1,'msg' => '操作成功','data'  =>'');
        $this->ajaxReturn($return_arr);
    }



    function import_excel(){
        set_time_limit(0);
        $exts = explode('.',$_FILES['file']['name'])[1];
        vendor('PHPExcel.Classes.PHPExcel');
        vendor('PHPExcel.Classes.PHPExcel.IOFactory');
        $objRead = new \PHPExcel();
        if($exts == 'xls'){
            vendor("PHPExcel.Classes.PHPExcel.Reader.Excel5");
            $objRead = new \PHPExcel_Reader_Excel5();
        }else if($exts == 'xlsx'){
            vendor("PHPExcel.Classes.PHPExcel.Reader.Excel2007");
            $objRead = new \PHPExcel_Reader_Excel2007();
        }
        $filename = $_FILES['file']['tmp_name'];
        $obj = $objRead->load($filename);
        $data = $obj->getActiveSheet()->toArray(null,true,true,true);
        if($data){
            $this->add($data);
        }
    }

    public function export_user()
    {
        $strTable = '<table width="500" border="1">';
        $strTable .= '<tr>';
        $strTable .= '<td style="text-align:center;font-size:12px;width:120px;">用户名</td>';
        $strTable .= '<td style="text-align:center;font-size:12px;width:120px;">电话</td>';
        $strTable .= '<td style="text-align:center;font-size:12px;">地址</td>';
        $strTable .= '</tr>';
        $users  = M('users')->where('1=1')->order('add_time asc')->select();
        if($users){
            if (is_array($users)) {
                foreach ($users as $k => $val) {
                    $strTable .= '<tr>';
                    $strTable .= '<td style="text-align:left;font-size:12px;">' . $val['username'] . ' </td>';
                    $strTable .= '<td style="text-align:left;font-size:12px;vnd.ms-excel.numberformat:@">' . $val['phone'] . ' </td>';
                    $strTable .= '<td style="text-align:left;font-size:12px;">' . $val['address'] . '</td>';
                    $strTable .= '</tr>';
                }
            }
            $strTable .= '</table>';
            downloadExcel($strTable, 'users');
        }
        exit();
    }

}

?>