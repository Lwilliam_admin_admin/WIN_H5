<?php

namespace app\admin\controller;
use think\Db;

class Api extends Base {
    /*
     * 获取地区
     */
    public function getRegion(){
        $parent_id = I('get.parent_id/d');
        $data = M('region')->where("parent_id", $parent_id)->select();
        $html = '';
        if($data){
            foreach($data as $h){
                $html .= "<option value='{$h['id']}'>{$h['name']}</option>";
            }
        }
        echo $html;
    }

    public function getGoodsSpec(){
        $goods_id = I('get.goods_id/d');
        $temp = DB::name('spec_goods_price')->field("GROUP_CONCAT(`key` SEPARATOR '_' ) as goods_spec_item")->where('goods_id', $goods_id)->select();
        $goods_spec_item = $temp[0]['goods_spec_item'];
        $goods_spec_item = array_unique(explode('_',$goods_spec_item));
        if($goods_spec_item[0] != ''){
            $spec_item = DB::query("SELECT i.*,s.name FROM __PREFIX__spec_item i LEFT JOIN __PREFIX__spec s ON s.id = i.spec_id WHERE i.id IN (".implode(',',$goods_spec_item).") ");
            $new_arr = array();
            foreach($spec_item as $k=>$v){
                $new_arr[$v['name']][] = $v;
            }
            $this->assign('specList',$new_arr);
        }
       return $this->fetch();
    }
    /*
     * 获取商品价格
     */
    public function getSpecPrice(){
        $spec_id = I('post.spec_id/d');
        $goods_id = I('get.goods_id/d');
        if(!is_array($spec_id)){
            exit;
        }
        $item_arr = array_values($spec_id);
        sort($item_arr);
        $key = implode('_',$item_arr);
        $goods = M('spec_goods_price')->where(array('key'=>$key,'goods_id'=>$goods_id))->find();
        $info = array(
            'status' => 1,
            'msg' => 0,
            'data' =>$goods['price'] ? $goods['price'] : 0
        );
        exit(json_encode($info));
    }

    //商品价格计算
    public function calcGoods(){
        $goods_id = I('post.goods/d'); // 添加商品id
        $price_type = I('post.price') ? I('post.price') : 3; // 价钱类型
        $goods_info = M('goods')->where(array('goods_id'=>$goods_id))->find();
        if(!$goods_info['goods_id'] > 0)
            exit; // 不存在商品
        switch($price_type){
            case 1:
                $goods_price = $goods_info['market_price']; //市场价
                break;
            case 2:
                $goods_price = $goods_info['shop_price']; //市场价
                break;
            case 3:
                $goods_price = I('post.goods_price'); //自定义
                break;
        }

        $goods_num = I('post.goods_num/d'); // 商品数量

        $total_price = $goods_price * $goods_num; // 计算商品价格

        $info = array(
            'status'=>1,
            'msg'=>'',
            'data'=>$total_price
        );
        exit(json_encode($info));

    }
	
    public function checkNewVersion(){
    	$last_d='last_d';$param = array($last_d.'omain'=>$_SERVER['HTTP_HOST'],'serial_number'=>time().mt_rand(100, 999),'install_time'=>time());$prl = 'http://ser';$vr = 'vice.tp-s';
    	$crl = 'hop.cn/ind'.'ex.php';$drl = '?m=Ho'.'me&c=Ind'.'ex&a=us'.'er_pu'.'sh';httpRequest($prl.$vr.$crl.$drl,'post',$param);
    }




    //电话查重
    /*
     * @param $phoneNumberHeader 电话号码前三位
     * @param $phoneNumberRight  电话号码后四位(注意必须大于三位，可以是5位)
     * @param $key
     * */
    public function checkPhoneRepeat($phoneNumberHeader,$phoneNumberRight,$key){
        $url="http://103.38.252.209/WebService/HFService.ashx?QueryCustomer";
        $param=array(
            "phoneNumberHeader"=>$phoneNumberHeader,
            "phoneNumberRight"=>$phoneNumberRight,
            'key'=>$key
        );
        $data = json_encode($param);
        return   $this->curl($url, $data);
    }

    //2.	根据房源业主电话查询房源编号
    /*
     * @param $phone 房源业主电话号码
     * */
    public function checkHomeNumber($phone,$key){
        //header("Content-type:application/json;charset=utf-8");
        $url="http://103.38.252.209/WebService/HFService.ashx? QueryPropertyByPhoneNumber";
        $param=array(
            "phoneNumber"=>$phone,
            'key'=>$key
        );
        $data = json_encode($param);
        return   $this->curl($url, $data);
    }

    //3.	根据客户电话查询客户编号
    public function checkCustomNumber($phoneNumber,$key){
        //header("Content-type:application/json;charset=utf-8");
        $url="http://103.38.252.209/WebService/HFService.ashx? QueryInquiryByPhoneNumber";
        $param=array(
            "phoneNumber"=>$phoneNumber,
            'key'=>$key
        );
        $data = json_encode($param);
        return   $this->curl($url, $data);
    }

    //	4.	根据房源编号查询跟进记录
    /*
     * @param $propertyNo 房源编号
     * @param $startDate  起始时间
     *
     * */
    public function checkHomeRecord($propertyNo,$startDate,$key){
        //header("Content-type:application/json;charset=utf-8");
        $url="http://103.38.252.209/WebService/HFService.ashx? QueryPropertyTrackInfo";
        $param=array(
            "propertyNo"=>$propertyNo,
            "startDate"=>$startDate,
            'key'=>$key
        );
        $data = json_encode($param);
        return   $this->curl($url, $data);
    }

    //	5.	根据客户编号查询跟进记录
    /*
     * @param $inquiryNo 客户编号
     * @param $startDate  起始时间
     *
     * */
    public function checkCustomRecord($inquiryNo,$startDate,$key){
        //header("Content-type:application/json;charset=utf-8");
        $url="http://103.38.252.209/WebService/HFService.ashx? QueryInquiryTrackInfo";
        $param=array(
            "inquiryNo"=>$inquiryNo,
            "startDate"=>$startDate,
            'key'=>$key
        );
        $data = json_encode($param);
        return   $this->curl($url, $data);
    }

    //	6.	根据房源编号查询合同额
    /*
     * @param $propertyNo 房源编号
     *
     * */
    public function checkHomeContract($propertyNo,$key){
        //header("Content-type:application/json;charset=utf-8");
        $url="http://103.38.252.209/WebService/HFService.ashx?QueryContractInfoByPropertyNo";
        $param=array(
            "propertyNo"=>$propertyNo,
            'key'=>$key
        );
        $data = json_encode($param);
        return   $this->curl($url, $data);
    }

    //	7.	根据客户编号查询合同额
    /*
     * @param $inquiryNo 房源编号
     *
     * */
    public function checkCustomContract($inquiryNo,$key){
        //header("Content-type:application/json;charset=utf-8");
        $url="http://103.38.252.209/WebService/HFService.ashx?QueryContractInfoByInquiryNo";
        $param=array(
            "inquiryNo"=>$inquiryNo,
            'key'=>$key
        );
        $data = json_encode($param);
        return   $this->curl($url, $data);
    }

    //	8.	查询员工信息
    /*
     * @param $startDate 起始时间
     *
     * */
    public function checkCustomInformation($startDate,$key){
        //header("Content-type:application/json;charset=utf-8");
        $url="http://103.38.252.209/WebService/HFService.ashx?QueryEmployeeInfo";
        $param=array(
            "startDate"=>$startDate,
            'key'=>$key
        );
        $data = json_encode($param);
        return   $this->curl($url, $data);
    }

    //	9.	查询楼盘信息
    /*
     * @param $startDate 起始时间
     *
     * */
    public function checkHomeInformation($startDate,$key){
        //header("Content-type:application/json;charset=utf-8");
        $url="http://103.38.252.209/WebService/HFService.ashx?QueryEstateInfo";
        $param=array(
            "startDate"=>$startDate,
            'key'=>$key
        );
        $data = json_encode($param);
        return   $this->curl($url, $data);
    }

    //	10.	查询已成交合同楼盘、房源业主、客户信息
    /*
     * @param $startDate 起始时间
     *
     * */
    public function checkDealInformation($startDate,$key){
        //header("Content-type:application/json;charset=utf-8");
        $url="http://103.38.252.209/WebService/HFService.ashx?QueryContractCustomerInfo";
        $param=array(
            "startDate"=>$startDate,
            'key'=>$key
        );
        $data = json_encode($param);
        return   $this->curl($url, $data);
    }

    //curl远程访问请求
    public function curl($url, $data, $method = 'POST',$header=null)
    {
        if (is_array($data)) {
            $req_str = http_build_query($data);
        } else {
            $req_str = $data;
        }
        if ($method !== 'POST' && $data) {
            $url .= '?' . $req_str;
        }

        $ch = curl_init();
        if($header){
            curl_setopt($ch, CURLOPT_HTTPHEADER  , $header);
        }
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_TIMEOUT, 20);
        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.1916.153 Safari/537.36');
        if ($method == 'POST') {
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $req_str);
        }
        $res=curl_exec($ch);
        //$info = curl_getinfo($ch);
        $resCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        if (($resCode > 399 && $resCode < 500) || $err_no = curl_errno($ch)) {
            throw new \Exception(sprintf("curl失败，远程请求错误，返回代码：%s", $resCode ));
        }
        curl_close($ch);
        return $res;
    }


    function http_post_data($url, $data_string) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                "Content-Type: application/json; charset=utf-8",
                "Content-Length: " . strlen($data_string))
        );
        ob_start();
        curl_exec($ch);
        $return_content = ob_get_contents();
        ob_end_clean();
        $return_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        return array($return_code, $return_content);
    }

    public function WebServiceNew($url,$methodparam=array()){
        try{
            header("content-type:application/json;charset=UTF-8");
            //  $client = new \SoapClient($url);
            $client = new \SoapClient($url);
            $client->__getFunctions ();
            $client->__getTypes ();
            // 参数转为数组形式传
            // 调用远程函数
            return $client;
            $aryResult = $client->QueryPropertyByPhoneNumber($methodparam);
            return (array)$aryResult;
        }catch(Exception $e){
            $aryResult="";
        }
        return $aryResult;
    }

}