<?php

namespace app\admin\controller;

use think\AjaxPage;
use think\db;
use think\Cache;
use Think\Page;

class System extends Base
{
	/*
	 * 配置入口
	 */
	public function index()
	{
		/*配置列表*/
		$group_list = [
            'shop_info' => '网站信息',
        ];
		$inc_type =  I('get.inc_type','shop_info');
		$config = tpCache($inc_type);
        $config['begin'] = date('Y-m-d H:i:s',$config['begin']);
        $config['end'] = date('Y-m-d H:i:s',$config['end']);
        $config['web_url'] = SITE_URL;
		return $this->fetch($inc_type,['config'=>$config,'inc_type'=>$inc_type,'group_list'=>$group_list]);
	}
	/*
	 * 新增修改配置
	 */
	public function handle()
	{
		$param = I('post.');
		$inc_type = $param['inc_type'];
		//unset($param['__hash__']);
		unset($param['inc_type']);
        $begin = strtotime($param['begin']);
        $end = strtotime($param['end']);
        $param['begin'] = strtotime(date('Y-m-d 00:00:00',$begin));
        $param['end'] = strtotime(date('Y-m-d 23:59:59',$end));
        if($param['begin'] > $param['end']){
            $this->error('开始时间必须小于结束时间');
        }
        delFile(RUNTIME_PATH);
		tpCache($inc_type,$param);
		$dir  = dirname( dirname( dirname(__DIR__) ) );
        $path = $dir."/public/ewm/";
        $fileName = $path.'ewm.png';
        if(file_exists($fileName)){
            unlink($fileName);
        }
        $url = tpCache("shop_info.web_url");
        qrcode_one($path,$url);
		$this->success("操作成功",U('System/index',array('inc_type'=>$inc_type)));
	}        
        

	public function refreshMenu(){
		$pmenu = $arr = array();
		$rs = M('system_module')->where('level>1 AND visible=1')->order('mod_id ASC')->select();
		foreach($rs as $row){
			if($row['level'] == 2){
				$pmenu[$row['mod_id']] = $row['title'];//父菜单
			}
		}
		foreach ($rs as $val){
			if($row['level']==2){
				$arr[$val['mod_id']] = $val['title'];
			}
			if($row['level']==3){
				$arr[$val['mod_id']] = $pmenu[$val['parent_id']].'/'.$val['title'];
			}
		}
		return $arr;
	}
        
        /**
         * 清空系统缓存
         */
        public function cleanCache(){
            delFile(RUNTIME_PATH);
            Cache::clear();
            $this->ajaxReturn(array('status' => 1, 'msg' => '正在清除缓存......'));
            exit();
        }
	    


     function right_list(){
     	$group = array(
			'system'=>'系统设置',
			'tools'=>'插件工具',
     	);
     	
     	$name = I('name');
     	if($name){
     	    $condition['name|right'] = array('like',"%$name%");
     	    $right_list = M('system_menu')->where($condition)->order('id desc')->select();
     	}else{
     	    $right_list = M('system_menu')->order('id desc')->select();
     	}
     	
     	$this->assign('right_list',$right_list);
     	$this->assign('group',$group);
     	return $this->fetch();
     }
     
     public function edit_right(){
     	if(IS_POST){
     		$data = I('post.');
     		$data['right'] = implode(',',$data['right']);
     		if(!empty($data['id'])){
     			M('system_menu')->where(array('id'=>$data['id']))->save($data);
     		}else{
     			if(M('system_menu')->where(array('name'=>$data['name']))->count()>0){
     				$this->error('该权限名称已添加，请检查',U('System/right_list'));
     			}
     			unset($data['id']);
     			M('system_menu')->add($data);
     		}
     		$this->success('操作成功',U('System/right_list'));
     		exit;
     	}
     	$id = I('id');
     	if($id){
     		$info = M('system_menu')->where(array('id'=>$id))->find();
     		$info['right'] = explode(',', $info['right']);
     		$this->assign('info',$info);
     	}
     	$group = array('system'=>'系统设置','content'=>'内容管理','goods'=>'商品中心','member'=>'会员中心',
     			'order'=>'订单中心','tools'=>'插件工具'
     	);
     	$planPath = APP_PATH.'admin/controller';
     	$planList = array();
     	$dirRes   = opendir($planPath);
     	while($dir = readdir($dirRes))
     	{
     		if(!in_array($dir,array('.','..','.svn')))
     		{
     			$planList[] = basename($dir,'.php');
     		}
     	}
     	$this->assign('planList',$planList);
     	$this->assign('group',$group);
        return $this->fetch();
     }
     
     public function right_del(){
     	$id = I('del_id');
     	if(is_array($id)){
     		$id = implode(',', $id); 
     	}
     	if(!empty($id)){
     		$r = M('system_menu')->where("id in ($id)")->delete();
     		if($r){
     			respose(1);
     		}else{
     			respose('删除失败');
     		}
     	}else{
     		respose('参数有误');
     	}
     }
}