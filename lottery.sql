/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50519
Source Host           : localhost:3306
Source Database       : lottery

Target Server Type    : MYSQL
Target Server Version : 50519
File Encoding         : 65001

Date: 2019-05-14 17:52:52
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for show_admin
-- ----------------------------
DROP TABLE IF EXISTS `show_admin`;
CREATE TABLE `show_admin` (
  `admin_id` smallint(5) unsigned NOT NULL AUTO_INCREMENT COMMENT '用户id',
  `user_name` varchar(60) NOT NULL DEFAULT '' COMMENT '用户名',
  `email` varchar(60) NOT NULL DEFAULT '' COMMENT 'email',
  `password` varchar(32) NOT NULL DEFAULT '' COMMENT '密码',
  `ec_salt` varchar(10) DEFAULT NULL COMMENT '秘钥',
  `add_time` int(11) NOT NULL DEFAULT '0' COMMENT '添加时间',
  `last_login` int(11) NOT NULL DEFAULT '0' COMMENT '最后登录时间',
  `last_ip` varchar(15) NOT NULL DEFAULT '' COMMENT '最后登录ip',
  `nav_list` text NOT NULL COMMENT '权限',
  `lang_type` varchar(50) NOT NULL DEFAULT '' COMMENT 'lang_type',
  `agency_id` smallint(5) unsigned NOT NULL COMMENT 'agency_id',
  `suppliers_id` smallint(5) unsigned DEFAULT '0' COMMENT 'suppliers_id',
  `todolist` longtext COMMENT 'todolist',
  `role_id` smallint(5) DEFAULT NULL COMMENT '角色id',
  PRIMARY KEY (`admin_id`),
  KEY `user_name` (`user_name`) USING BTREE,
  KEY `agency_id` (`agency_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of show_admin
-- ----------------------------
INSERT INTO `show_admin` VALUES ('1', 'admin', '1475924013@qq.com', 'a4abfed2ab6f5bf97b2c56434e2f0695', null, '1504508804', '1557817817', '118.114.6.151', '', '', '0', '0', null, '1');

-- ----------------------------
-- Table structure for show_admin_log
-- ----------------------------
DROP TABLE IF EXISTS `show_admin_log`;
CREATE TABLE `show_admin_log` (
  `log_id` bigint(16) unsigned NOT NULL AUTO_INCREMENT COMMENT '表id',
  `admin_id` int(10) DEFAULT NULL COMMENT '管理员id',
  `log_info` varchar(255) DEFAULT NULL COMMENT '日志描述',
  `log_ip` varchar(30) DEFAULT NULL COMMENT 'ip地址',
  `log_url` varchar(50) DEFAULT NULL COMMENT 'url',
  `log_time` int(10) DEFAULT NULL COMMENT '日志时间',
  PRIMARY KEY (`log_id`)
) ENGINE=InnoDB AUTO_INCREMENT=651 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of show_admin_log
-- ----------------------------
INSERT INTO `show_admin_log` VALUES ('640', '1', '后台登录', '127.0.0.1', '/Admin/Admin/login', '1504509428');
INSERT INTO `show_admin_log` VALUES ('641', '1', '后台登录', '127.0.0.1', '/Admin/Admin/login', '1504509893');
INSERT INTO `show_admin_log` VALUES ('642', '1', '后台登录', '118.112.204.64', '/Admin/Admin/login', '1504520140');
INSERT INTO `show_admin_log` VALUES ('643', '1', '后台登录', '118.112.204.64', '/Admin/Admin/login', '1504595546');
INSERT INTO `show_admin_log` VALUES ('644', '1', '后台登录', '182.148.73.181', '/Admin/Admin/login', '1504622722');
INSERT INTO `show_admin_log` VALUES ('645', '1', '后台登录', '118.112.204.64', '/Admin/Admin/login', '1504662737');
INSERT INTO `show_admin_log` VALUES ('646', '1', '后台登录', '118.112.204.64', '/Admin/Admin/login', '1504676420');
INSERT INTO `show_admin_log` VALUES ('647', '1', '后台登录', '118.112.204.64', '/Admin/Admin/login', '1504679889');
INSERT INTO `show_admin_log` VALUES ('648', '1', '后台登录', '127.0.0.1', '/index.php', '1557711277');
INSERT INTO `show_admin_log` VALUES ('649', '1', '后台登录', '118.114.5.166', '/index.php', '1557741809');
INSERT INTO `show_admin_log` VALUES ('650', '1', '后台登录', '118.114.6.151', '/index.php', '1557817817');

-- ----------------------------
-- Table structure for show_admin_role
-- ----------------------------
DROP TABLE IF EXISTS `show_admin_role`;
CREATE TABLE `show_admin_role` (
  `role_id` smallint(6) unsigned NOT NULL AUTO_INCREMENT COMMENT '角色ID',
  `role_name` varchar(30) DEFAULT NULL COMMENT '角色名称',
  `act_list` text COMMENT '权限列表',
  `role_desc` varchar(255) DEFAULT NULL COMMENT '角色描述',
  PRIMARY KEY (`role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of show_admin_role
-- ----------------------------
INSERT INTO `show_admin_role` VALUES ('1', '超级管理员', 'all', '管理全站');

-- ----------------------------
-- Table structure for show_config
-- ----------------------------
DROP TABLE IF EXISTS `show_config`;
CREATE TABLE `show_config` (
  `id` smallint(6) unsigned NOT NULL AUTO_INCREMENT COMMENT '表id',
  `name` varchar(64) DEFAULT NULL COMMENT '配置的key键名',
  `value` varchar(512) DEFAULT NULL COMMENT '配置的val值',
  `inc_type` varchar(64) DEFAULT NULL COMMENT '配置分组',
  `desc` varchar(50) DEFAULT NULL COMMENT '描述',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=85 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of show_config
-- ----------------------------
INSERT INTO `show_config` VALUES ('76', 'web_url', 'http://lottery.newzow.com', 'shop_info', null);
INSERT INTO `show_config` VALUES ('77', 'web_name', '平安答题', 'shop_info', null);
INSERT INTO `show_config` VALUES ('78', 'thinks', '0.02', 'shop_info', null);
INSERT INTO `show_config` VALUES ('79', 'form_submit', 'ok', 'shop_info', null);
INSERT INTO `show_config` VALUES ('80', 'close_open', '1', 'shop_info', null);
INSERT INTO `show_config` VALUES ('81', 'store_title', '', 'shop_info', null);
INSERT INTO `show_config` VALUES ('82', 'notice', '奖品已经领取完，谢谢您的支持', 'shop_info', null);
INSERT INTO `show_config` VALUES ('83', 'begin', '1557417600', 'shop_info', null);
INSERT INTO `show_config` VALUES ('84', 'end', '1559318399', 'shop_info', null);

-- ----------------------------
-- Table structure for show_gift
-- ----------------------------
DROP TABLE IF EXISTS `show_gift`;
CREATE TABLE `show_gift` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `gold_name` varchar(32) DEFAULT NULL,
  `number` int(11) DEFAULT '0' COMMENT '剩余数量',
  `all_number` int(11) DEFAULT '0' COMMENT '总数',
  `pic` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of show_gift
-- ----------------------------
INSERT INTO `show_gift` VALUES ('2', '小米全高清40寸大彩电', '1', '1', '/public/upload/gift/2019/05-14/2f79d362a5a0e119187d630960043b34.jpg');
INSERT INTO `show_gift` VALUES ('3', '小米电风扇', '1', '1', '/public/upload/gift/2019/05-14/30e373ba9cc5d28c94251a6f24dea748.jpg');
INSERT INTO `show_gift` VALUES ('4', '飞利浦电饭煲', '1', '1', '/public/upload/gift/2019/05-14/051cc99d38ea638d45739e519ef4dff2.jpg');
INSERT INTO `show_gift` VALUES ('5', '小米电动牙刷', '2', '2', '/public/upload/gift/2019/05-14/b403400925245f9f35a9a4a230ffdb85.jpg');
INSERT INTO `show_gift` VALUES ('6', '小米枕头', '2', '2', '/public/upload/gift/2019/05-14/8861afc30cee20531108351b10a2f18e.jpg');
INSERT INTO `show_gift` VALUES ('7', '东菱榨汁机', '1', '1', '/public/upload/gift/2019/05-14/764eee9f073b8c9c9d84a7bdb036f040.jpg');
INSERT INTO `show_gift` VALUES ('8', '蓝牙耳机', '5', '7', '/public/upload/gift/2019/05-14/f1ddeee07b92272fb38a63fac90c888e.jpg');
INSERT INTO `show_gift` VALUES ('9', 'U盘', '7', '7', '/public/upload/gift/2019/05-14/5f20fd40f0359c1480050c3725f4eff5.jpg');
INSERT INTO `show_gift` VALUES ('10', '手持电风扇', '4', '6', '/public/upload/gift/2019/05-14/bd834fcce7ff7f6d9fb5394d53d95935.jpg');
INSERT INTO `show_gift` VALUES ('11', '充电宝', '69', '72', '/public/upload/gift/2019/05-14/34dacc6ca2e63900bbde3648302c5cd4.jpg');

-- ----------------------------
-- Table structure for show_wx_config
-- ----------------------------
DROP TABLE IF EXISTS `show_wx_config`;
CREATE TABLE `show_wx_config` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '表id',
  `uid` int(11) NOT NULL COMMENT 'uid',
  `wxname` varchar(60) NOT NULL COMMENT '公众号名称',
  `aeskey` varchar(256) NOT NULL DEFAULT '' COMMENT 'aeskey',
  `encode` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'encode',
  `appid` varchar(50) NOT NULL DEFAULT '' COMMENT 'appid',
  `appsecret` varchar(50) NOT NULL DEFAULT '' COMMENT 'appsecret',
  `wxid` varchar(64) NOT NULL COMMENT '公众号原始ID',
  `weixin` char(64) NOT NULL COMMENT '微信号',
  `headerpic` char(255) NOT NULL COMMENT '头像地址',
  `token` char(255) NOT NULL COMMENT 'token',
  `w_token` varchar(150) NOT NULL DEFAULT '' COMMENT '微信对接token',
  `create_time` int(11) NOT NULL COMMENT 'create_time',
  `updatetime` int(11) NOT NULL COMMENT 'updatetime',
  `tplcontentid` varchar(2) NOT NULL COMMENT '内容模版ID',
  `share_ticket` varchar(150) NOT NULL COMMENT '分享ticket',
  `share_dated` char(15) NOT NULL COMMENT 'share_dated',
  `authorizer_access_token` varchar(200) NOT NULL COMMENT 'authorizer_access_token',
  `authorizer_refresh_token` varchar(200) NOT NULL COMMENT 'authorizer_refresh_token',
  `authorizer_expires` char(10) NOT NULL COMMENT 'authorizer_expires',
  `type` tinyint(1) NOT NULL DEFAULT '0' COMMENT '类型',
  `web_access_token` varchar(200) NOT NULL COMMENT ' 网页授权token',
  `web_refresh_token` varchar(200) NOT NULL COMMENT 'web_refresh_token',
  `web_expires` int(11) NOT NULL COMMENT '过期时间',
  `qr` varchar(200) NOT NULL COMMENT 'qr',
  `menu_config` text COMMENT '菜单',
  `wait_access` tinyint(1) DEFAULT '0' COMMENT '微信接入状态,0待接入1已接入',
  PRIMARY KEY (`id`),
  KEY `uid` (`uid`) USING BTREE,
  KEY `uid_2` (`uid`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='微信公共帐号';

-- ----------------------------
-- Records of show_wx_config
-- ----------------------------
INSERT INTO `show_wx_config` VALUES ('1', '0', '超级营销管家', 'jKxqCBzzkKDvh054RQShgdpKz2Wm0FLggjk045d3Q2k', '0', 'wx70a7502418f2e3da', 'a9667ada621467d77f5af1dba6088ab1', ' gh_669ed49c7c9', '', '', 'Pyc833crcO6qLQZ1E88QXR1EV36q3Z8L', 'fgNG51O0shOgwsW7c2185N7O40S8151C', '0', '0', '', '', '', '', '', '', '4', 'twPEWbFk7W1xH5rhBh4y-z0bm7M9EkDARhnMfWdg7LJaEchlzM_tVdVKlj7LpHojKQY18ebdPhkK2to3puMOd0C75QHmfiiqxK9cEBvEOpnbOBiUKsRki3m1DyGnRgEbUXGhADAQTC', '', '0', '', null, '1');

-- ----------------------------
-- Table structure for show_wx_user
-- ----------------------------
DROP TABLE IF EXISTS `show_wx_user`;
CREATE TABLE `show_wx_user` (
  `user_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `openid` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `headerpic` varchar(255) DEFAULT '' COMMENT '头像地址',
  `wx_info` varchar(255) DEFAULT NULL,
  `token` varchar(255) DEFAULT NULL,
  `last_login` varchar(255) DEFAULT NULL,
  `reg_time` varchar(255) DEFAULT '0',
  `flag` tinyint(3) DEFAULT '0' COMMENT '是否参与过',
  `gold_name` varchar(255) DEFAULT NULL,
  `status` tinyint(3) DEFAULT '0' COMMENT '是否中奖',
  `play_time` varchar(255) DEFAULT NULL,
  `gid` int(10) DEFAULT NULL,
  `session_ids` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `uid` (`openid`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of show_wx_user
-- ----------------------------
INSERT INTO `show_wx_user` VALUES ('10', 'oIsuIw_jyWAHwtwMDAwjCx1FU8Cs', null, '', null, null, null, '1557797703', '0', null, '0', null, '0', '');
INSERT INTO `show_wx_user` VALUES ('11', 'oIsuIw5U7uNELCsfdrEdu88coPGo', null, '', null, null, null, '1557802781', '0', null, '0', null, '0', '');
INSERT INTO `show_wx_user` VALUES ('13', 'oIsuIw6f4w-9-iOdIR7mXqQh0ABk', null, '', null, null, null, '1557819974', '0', null, '0', null, '0', '');
INSERT INTO `show_wx_user` VALUES ('12', 'oIsuIw4-i1VsT3IwxYQuz4dj20Xc', null, '', null, null, null, '1557818069', '1', '蓝牙耳机', '1', '1557826836', '8', '1557826664');
