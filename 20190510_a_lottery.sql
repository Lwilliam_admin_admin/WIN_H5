/*
Navicat MySQL Data Transfer

Source Server         : lgw
Source Server Version : 80012
Source Host           : localhost:3306
Source Database       : 20190510_a_lottery

Target Server Type    : MYSQL
Target Server Version : 80012
File Encoding         : 65001

Date: 2019-05-13 17:51:53
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for show_admin
-- ----------------------------
DROP TABLE IF EXISTS `show_admin`;
CREATE TABLE `show_admin` (
  `admin_id` smallint(5) unsigned NOT NULL AUTO_INCREMENT COMMENT '用户id',
  `user_name` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '用户名',
  `email` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT 'email',
  `password` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '密码',
  `ec_salt` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '秘钥',
  `add_time` int(11) NOT NULL DEFAULT '0' COMMENT '添加时间',
  `last_login` int(11) NOT NULL DEFAULT '0' COMMENT '最后登录时间',
  `last_ip` varchar(15) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '最后登录ip',
  `nav_list` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '权限',
  `lang_type` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT 'lang_type',
  `agency_id` smallint(5) unsigned NOT NULL COMMENT 'agency_id',
  `suppliers_id` smallint(5) unsigned DEFAULT '0' COMMENT 'suppliers_id',
  `todolist` longtext CHARACTER SET utf8 COLLATE utf8_general_ci COMMENT 'todolist',
  `role_id` smallint(5) DEFAULT NULL COMMENT '角色id',
  PRIMARY KEY (`admin_id`),
  KEY `user_name` (`user_name`) USING BTREE,
  KEY `agency_id` (`agency_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of show_admin
-- ----------------------------
INSERT INTO `show_admin` VALUES ('1', 'admin', '1475924013@qq.com', 'a4abfed2ab6f5bf97b2c56434e2f0695', null, '1504508804', '1557711277', '127.0.0.1', '', '', '0', '0', null, '1');

-- ----------------------------
-- Table structure for show_admin_log
-- ----------------------------
DROP TABLE IF EXISTS `show_admin_log`;
CREATE TABLE `show_admin_log` (
  `log_id` bigint(16) unsigned NOT NULL AUTO_INCREMENT COMMENT '表id',
  `admin_id` int(10) DEFAULT NULL COMMENT '管理员id',
  `log_info` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '日志描述',
  `log_ip` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT 'ip地址',
  `log_url` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT 'url',
  `log_time` int(10) DEFAULT NULL COMMENT '日志时间',
  PRIMARY KEY (`log_id`)
) ENGINE=InnoDB AUTO_INCREMENT=649 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of show_admin_log
-- ----------------------------
INSERT INTO `show_admin_log` VALUES ('640', '1', '后台登录', '127.0.0.1', '/Admin/Admin/login', '1504509428');
INSERT INTO `show_admin_log` VALUES ('641', '1', '后台登录', '127.0.0.1', '/Admin/Admin/login', '1504509893');
INSERT INTO `show_admin_log` VALUES ('642', '1', '后台登录', '118.112.204.64', '/Admin/Admin/login', '1504520140');
INSERT INTO `show_admin_log` VALUES ('643', '1', '后台登录', '118.112.204.64', '/Admin/Admin/login', '1504595546');
INSERT INTO `show_admin_log` VALUES ('644', '1', '后台登录', '182.148.73.181', '/Admin/Admin/login', '1504622722');
INSERT INTO `show_admin_log` VALUES ('645', '1', '后台登录', '118.112.204.64', '/Admin/Admin/login', '1504662737');
INSERT INTO `show_admin_log` VALUES ('646', '1', '后台登录', '118.112.204.64', '/Admin/Admin/login', '1504676420');
INSERT INTO `show_admin_log` VALUES ('647', '1', '后台登录', '118.112.204.64', '/Admin/Admin/login', '1504679889');
INSERT INTO `show_admin_log` VALUES ('648', '1', '后台登录', '127.0.0.1', '/index.php', '1557711277');

-- ----------------------------
-- Table structure for show_admin_role
-- ----------------------------
DROP TABLE IF EXISTS `show_admin_role`;
CREATE TABLE `show_admin_role` (
  `role_id` smallint(6) unsigned NOT NULL AUTO_INCREMENT COMMENT '角色ID',
  `role_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '角色名称',
  `act_list` text CHARACTER SET utf8 COLLATE utf8_general_ci COMMENT '权限列表',
  `role_desc` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '角色描述',
  PRIMARY KEY (`role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of show_admin_role
-- ----------------------------
INSERT INTO `show_admin_role` VALUES ('1', '超级管理员', 'all', '管理全站');

-- ----------------------------
-- Table structure for show_answer_log
-- ----------------------------
DROP TABLE IF EXISTS `show_answer_log`;
CREATE TABLE `show_answer_log` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `period_id` int(10) NOT NULL,
  `wx_user_id` int(10) NOT NULL,
  `gold` tinyint(1) DEFAULT '0' COMMENT '是否已经抽过奖',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of show_answer_log
-- ----------------------------
INSERT INTO `show_answer_log` VALUES ('3', '3', '3', '0');
INSERT INTO `show_answer_log` VALUES ('4', '3', '3', '0');
INSERT INTO `show_answer_log` VALUES ('5', '3', '3', '0');
INSERT INTO `show_answer_log` VALUES ('8', '3', '2', '1');
INSERT INTO `show_answer_log` VALUES ('9', '4', '3', '1');
INSERT INTO `show_answer_log` VALUES ('10', '4', '4', '0');
INSERT INTO `show_answer_log` VALUES ('11', '4', '6', '0');
INSERT INTO `show_answer_log` VALUES ('12', '4', '8', '0');
INSERT INTO `show_answer_log` VALUES ('13', '5', '8', '1');
INSERT INTO `show_answer_log` VALUES ('14', '5', '3', '1');
INSERT INTO `show_answer_log` VALUES ('15', '6', '3', '1');

-- ----------------------------
-- Table structure for show_config
-- ----------------------------
DROP TABLE IF EXISTS `show_config`;
CREATE TABLE `show_config` (
  `id` smallint(6) unsigned NOT NULL AUTO_INCREMENT COMMENT '表id',
  `name` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '配置的key键名',
  `value` varchar(512) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '配置的val值',
  `inc_type` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '配置分组',
  `desc` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '描述',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=85 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of show_config
-- ----------------------------
INSERT INTO `show_config` VALUES ('76', 'web_url', 'http://ex.lottery.com', 'shop_info', null);
INSERT INTO `show_config` VALUES ('77', 'web_name', '平安答题', 'shop_info', null);
INSERT INTO `show_config` VALUES ('78', 'thinks', '0.02', 'shop_info', null);
INSERT INTO `show_config` VALUES ('79', 'form_submit', 'ok', 'shop_info', null);
INSERT INTO `show_config` VALUES ('80', 'close_open', '1', 'shop_info', null);
INSERT INTO `show_config` VALUES ('81', 'store_title', '', 'shop_info', null);
INSERT INTO `show_config` VALUES ('82', 'notice', '奖品已经领取完，谢谢您的支持', 'shop_info', null);
INSERT INTO `show_config` VALUES ('83', 'begin', '1557417600', 'shop_info', null);
INSERT INTO `show_config` VALUES ('84', 'end', '1559318399', 'shop_info', null);

-- ----------------------------
-- Table structure for show_gift
-- ----------------------------
DROP TABLE IF EXISTS `show_gift`;
CREATE TABLE `show_gift` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `gold_name` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `number` int(11) DEFAULT '0' COMMENT '剩余数量',
  `all_number` int(11) DEFAULT '0' COMMENT '总数',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of show_gift
-- ----------------------------
INSERT INTO `show_gift` VALUES ('2', '小米全高清40寸大彩电', '1', '1');
INSERT INTO `show_gift` VALUES ('3', '小米电风扇', '1', '1');
INSERT INTO `show_gift` VALUES ('4', '飞利浦电饭煲', '1', '1');
INSERT INTO `show_gift` VALUES ('5', '小米电动牙刷', '2', '2');
INSERT INTO `show_gift` VALUES ('6', '小米枕头', '2', '2');
INSERT INTO `show_gift` VALUES ('7', '东菱榨汁机', '1', '1');
INSERT INTO `show_gift` VALUES ('8', '蓝牙耳机', '7', '7');
INSERT INTO `show_gift` VALUES ('9', 'U盘', '7', '7');
INSERT INTO `show_gift` VALUES ('10', '手持电风扇', '6', '6');
INSERT INTO `show_gift` VALUES ('11', '充电宝', '72', '72');

-- ----------------------------
-- Table structure for show_wx_config
-- ----------------------------
DROP TABLE IF EXISTS `show_wx_config`;
CREATE TABLE `show_wx_config` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '表id',
  `uid` int(11) NOT NULL COMMENT 'uid',
  `wxname` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '公众号名称',
  `aeskey` varchar(256) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT 'aeskey',
  `encode` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'encode',
  `appid` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT 'appid',
  `appsecret` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT 'appsecret',
  `wxid` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '公众号原始ID',
  `weixin` char(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '微信号',
  `headerpic` char(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '头像地址',
  `token` char(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'token',
  `w_token` varchar(150) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '微信对接token',
  `create_time` int(11) NOT NULL COMMENT 'create_time',
  `updatetime` int(11) NOT NULL COMMENT 'updatetime',
  `tplcontentid` varchar(2) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '内容模版ID',
  `share_ticket` varchar(150) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '分享ticket',
  `share_dated` char(15) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'share_dated',
  `authorizer_access_token` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'authorizer_access_token',
  `authorizer_refresh_token` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'authorizer_refresh_token',
  `authorizer_expires` char(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'authorizer_expires',
  `type` tinyint(1) NOT NULL DEFAULT '0' COMMENT '类型',
  `web_access_token` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT ' 网页授权token',
  `web_refresh_token` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'web_refresh_token',
  `web_expires` int(11) NOT NULL COMMENT '过期时间',
  `qr` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'qr',
  `menu_config` text CHARACTER SET utf8 COLLATE utf8_general_ci COMMENT '菜单',
  `wait_access` tinyint(1) DEFAULT '0' COMMENT '微信接入状态,0待接入1已接入',
  PRIMARY KEY (`id`),
  KEY `uid` (`uid`) USING BTREE,
  KEY `uid_2` (`uid`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='微信公共帐号';

-- ----------------------------
-- Records of show_wx_config
-- ----------------------------
INSERT INTO `show_wx_config` VALUES ('1', '0', '超级营销管家', 'jKxqCBzzkKDvh054RQShgdpKz2Wm0FLggjk045d3Q2k', '0', 'wx70a7502418f2e3da', 'a9667ada621467d77f5af1dba6088ab1', ' gh_669ed49c7c9', '', '', 'Pyc833crcO6qLQZ1E88QXR1EV36q3Z8L', 'fgNG51O0shOgwsW7c2185N7O40S8151C', '0', '0', '', '', '', '', '', '', '1', 'twPEWbFk7W1xH5rhBh4y-z0bm7M9EkDARhnMfWdg7LJaEchlzM_tVdVKlj7LpHojKQY18ebdPhkK2to3puMOd0C75QHmfiiqxK9cEBvEOpnbOBiUKsRki3m1DyGnRgEbUXGhADAQTC', '', '0', '', null, '1');

-- ----------------------------
-- Table structure for show_wx_user
-- ----------------------------
DROP TABLE IF EXISTS `show_wx_user`;
CREATE TABLE `show_wx_user` (
  `user_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `openid` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `username` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `headerpic` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '头像地址',
  `wx_info` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `token` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `last_login` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `reg_time` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '0',
  `flag` tinyint(3) DEFAULT '0' COMMENT '是否参与过',
  `gold_name` varchar(255) DEFAULT NULL,
  `status` tinyint(3) DEFAULT '0' COMMENT '是否中奖',
  `play_time` varchar(255) DEFAULT NULL,
  `gid` int(10) DEFAULT NULL,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `uid` (`openid`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of show_wx_user
-- ----------------------------
INSERT INTO `show_wx_user` VALUES ('2', 'on4D_wtpwPj86bru2VWe0dFU5PDY', 'william', 'http://wx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTIFPPQHJ54QtHciazicc1iadWcVOroG8LEnt1T9rQZb7RlDncpSRKem5hQJzvZkycjIB8CoeEPVP19ug/0', '{\"access_token\":\"fdc028x9GgXvhmA16Yv3ndPJFo7cz1HfDt63hdlBcv25rsfJO4jHgQTUp-UQNjodXCglRnLwwPoaA-h-vfuXiQ\",\"expires_in\":7200,\"refresh_token\":\"LumjL48wLJ1w-9gSXosjUxwjS2Ee-MSZu16ROBTQmUKjOSxGCGhn0uxWJZmfxIlnYJon264SG60Z0kWglQ__ZQ\",\"openid\":\"on4D_wtpwPj86bru2', null, null, '1504678850', '0', null, '0', null, null);
INSERT INTO `show_wx_user` VALUES ('3', 'on4D_wtW4uVFibElccuan6cu5fnI', '琼二', 'http://wx.qlogo.cn/mmopen/vi_32/bjyE8on7Qn98qDAPafcJqVdoCb8b4Q1wUrlVu8usSYSyK1ZPJQiacl8X1v7NWztbfVc9S9ia6o3c9Z4ia9vPRXia3g/0', '{\"access_token\":\"TVtPnzYEMKZ5OkHDXV5iEq1-tvXCIMpNe1BZFLG5s86BSfU8rMBq9QTb19mREa4D65WjcJxB5WoNBsdhvpozCQ\",\"expires_in\":7200,\"refresh_token\":\"UUVtLrUPWOxaiTxfahsvpK4UR-b_rW4iYUgzl5iBppomLwkzcM_KQFg9P2XC4gqcoNMxqjtanFawf7skujBLgw\",\"openid\":\"on4D_wtW4uVFibElc', null, null, '1504662160', '0', null, '0', null, null);
INSERT INTO `show_wx_user` VALUES ('4', 'on4D_wmHn9rAoyFWZ80HGPB6ZxtA', '完美电子签到18782911147', 'http://wx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTLgzaEmNllBibpVVUYeATSKCkiaO2ySYibuJXLSrSGLGF7R7NDAM2tkXR5QLy0BujeYZObiafoPwVOf3w/0', '{\"access_token\":\"ZDnAn5m5ghYmTtnvvMfCNwgU8xjsBWNl8mxvsQ3A-Lj_i-rd6l8-IidguPmR3kHRDczL4Z__yoypnas3Pqwqa0GG2a_ev-vEFEgbmryQYNM\",\"expires_in\":7200,\"refresh_token\":\"UQNldJoB4EnFk_yMWQAaB4qcipEJOI1Bw0CMgkiOnw8MmGdrJdVg_H1q2RC8OE9YoJp8PWnOvIz3DoZaIDiNC62X0vW7WH', null, null, '1504676798', '0', null, '0', null, null);
INSERT INTO `show_wx_user` VALUES ('5', 'on4D_wpVVQdur3It6dywUUeNeBFM', 'Forever', 'http://wx.qlogo.cn/mmopen/vi_32/DYAIOgq83eoDGe7wXlOa1ujia0jmiaDNKTKDrrwGAr17ia6ZZ9wJtKCJIap7Kibran6HlaDGYiaibib9G5wFyHBxhcRLg/0', '{\"access_token\":\"-bGwFgFyz_UKR1UVh6G8zr7zlp-viskRTsL8gWCM87biRAJSoQ4lRxIGOukMEP39HsgssMH7s3LtCzyP_GPD2g\",\"expires_in\":7200,\"refresh_token\":\"UkUASag3fucaeK_bRXjB6HX23vh829l5PC902uUAqTT9hEqG7hiYdSEktag2KHsIPXnLqs4Z4Vy35dbF6Z7H_Q\",\"openid\":\"on4D_wpVVQdur3It6', null, null, '1504677481', '0', null, '0', null, null);
INSERT INTO `show_wx_user` VALUES ('6', 'ojA_awDhKEkZK61jz_SSdRh_1q0o', 'william', 'http://wx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTIuj80OvNolRqlF7zBWCNlcyVnBhmvnbxVh7UCNicve3o3k7XRKrD6JvfyPKmdCzgpdSfraeyXcr7Q/0', '{\"access_token\":\"-O8vDEqVcPwSV6St8iUAdrhEZeDTahLnCYhbL8fsd6ORq0YGfPru85v-zwnF_9RCspWGG0jmS1Pm3RdyCsTG9g\",\"expires_in\":7200,\"refresh_token\":\"KhdJtcAWxY8Xu8LEPesT7c5GOQ4djnL3cgiVok6JeJCi6dFugnITkB7Gi1I_pXGToQi9d4t10542TNjoHXuCyg\",\"openid\":\"ojA_awDhKEkZK61jz', null, null, '1504682661', '0', null, '0', null, null);
INSERT INTO `show_wx_user` VALUES ('7', 'ojA_awK6ehYpuUhuov_57NfqSy-k', 'DaJin', 'http://wx.qlogo.cn/mmopen/Q3auHgzwzM6s8tSjBYzQ5PxUic8LJecTL8KHgpmWA1tb30VSU31phdHaYdcXClgkq74yzf2ajVRicA2e1hMJAD9BntnfVkGV20Y8d6XdXL8uk/0', '{\"subscribe\":1,\"openid\":\"ojA_awK6ehYpuUhuov_57NfqSy-k\",\"nickname\":\"DaJin\",\"sex\":1,\"language\":\"zh_CN\",\"city\":\"\\u5185\\u6c5f\",\"province\":\"\\u56db\\u5ddd\",\"country\":\"\\u4e2d\\u56fd\",\"headimgurl\":\"http:\\/\\/wx.qlogo.cn\\/mmopen\\/Q3auHgzwzM6s8tSjBYzQ5PxUic8LJecTL8KHg', null, null, '1504684331', '0', null, '0', null, null);
INSERT INTO `show_wx_user` VALUES ('8', 'ojA_awBsdW21oBUBTJwkcyqXsOQI', '完美电子签到18782911147', 'http://wx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTKK4YHjDlZicQ98aSOL2Mw48UwTEicb9wBxrsRQfTDJhej52CeibDJm2uH97y1Uck6mGpicgQ7nldyr0Q/0', '{\"access_token\":\"OBHXwbE24guUCrDnka54F6mSomPYiFx_ukKfghdIb1htx2Y2NdnPXeW_lYcNzND6C4mq1IJC9VOflyOzRsE8rPFOdzJQ2qDayuUstgc4yq8\",\"expires_in\":7200,\"refresh_token\":\"Yctx_pfKqqscBg7lH8a3iRW-qeY3sF1EvkC1DgCb6nsrJsYbb-xV0rsiRNiukQm5XZXTYnykTeFZTDhLZSsbFoWQgtBIc7', null, null, '1504685210', '0', null, '0', null, null);
INSERT INTO `show_wx_user` VALUES ('9', 'ojA_awCYRPkYpXR-4ZBUu_Eac6tg', '菠萝蜜', 'http://wx.qlogo.cn/mmopen/1CHHx9Yq4nGibaIvQFF6Rxg31uZlGCdMwibdcoiaCCTYicJIGsxMKLpypE4CoZTZibrf4cicgiahxK4sjBWzEIcPiawXmPXJSjAPQgM7/0', '{\"subscribe\":1,\"openid\":\"ojA_awCYRPkYpXR-4ZBUu_Eac6tg\",\"nickname\":\"\\u83e0\\u841d\\u871c\",\"sex\":0,\"language\":\"zh_CN\",\"city\":\"\",\"province\":\"\",\"country\":\"\",\"headimgurl\":\"http:\\/\\/wx.qlogo.cn\\/mmopen\\/1CHHx9Yq4nGibaIvQFF6Rxg31uZlGCdMwibdcoiaCCTYicJIGsxMKLpypE4C', null, null, '1504685948', '0', null, '0', null, null);
