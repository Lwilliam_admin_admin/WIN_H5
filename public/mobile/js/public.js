(function (doc, win) {
  var docEl = doc.documentElement,
      resizeEvt = 'orientationchange' in window ? 'orientationchange' : 'resize',
      recalc = function () {
        var clientWidth = docEl.clientWidth;
        if (!clientWidth) return;
        docEl.style.fontSize = 20*(clientWidth / 375) + 'px';
      };
  recalc();
  if (!doc.addEventListener) return;
  win.addEventListener(resizeEvt, recalc, false);
})(document, window);

var tips=function(text,fn){
    var html='<div class="layer_mask" id="layerMask"><div class="layer_mask_box fs24"><div class="layer_title">温馨提示</div><div class="layer_content">'+text+'</div><div class="btn_one" id="layerYesBtn">确定</div></div></div>';
    $("body").append(html);
    $("#layerYesBtn").on("click",function(){
      $("#layerMask").remove();
      if(fn){fn()}
    })
}
function getTime(time) {
    var timer=new Date(time*1000);
    var year=timer.getFullYear()
    var month=timer.getMonth()+1;
    month=month>9?month:"0"+month;
    var day=timer.getDate();
    day=day>9?day:"0"+day;
    return year+"-"+month+"-"+day;
}
//加载动画
window.showLoadMask=function(){
    var html='<div class="mask_load" id="maskLoad"><div class="mask_load_box"><div class="spinner"><div class="dot1"></div><div class="dot2"></div></div></div></div>'
        $("body").append(html)
}
//加载动画消失
window.hideLoadMask=function(){
    $("#maskLoad").remove();
}
//获取url传参
function GetRequest() {  
   var url = location.search;  
   var theRequest = {};   
   if (url.indexOf("?") != -1) {   
      var str = url.substr(1);   
      strs = str.split("&");   
      for(var i = 0; i < strs.length; i ++) {   
         theRequest[strs[i].split("=")[0]]=unescape(strs[i].split("=")[1]);   
      }   
   }   
   return theRequest;
}